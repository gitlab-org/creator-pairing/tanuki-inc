# Tanuki Inc

## Overview
This demo project is a showcase for features in the Monitor stage for the Operations section of GitLab. The project hosts the Tanuki Inc Ops demo application. 
The application is being monitored by Prometheus (GitLab managed on the same cluster as the application). Specific features that can be seen in action in this project:

- Metrics via Gitlab-managed prometheus displayed on the metrics dashboard
- Alerting: Create alerts for metrics via the metrics dashboard
- Triage alerts using the alerts list 
- Respond to critical alerts by promoting them to incidents
- Manage incidents using the incident list
- Publish incidents to a status page

**Disclaimer**: This project is publicly available as a live demo for the Monitor stage, however the environement is also actively used by product development teams at  GitLab for internal testing and feature assurance. State of the project is not guaranteed.

## How to use

### Generating Errors

1. Navigate to the [demo environment](https://gitlab-examples-ops-incident-setup-everyone-tanuki-inc.34.69.64.147.nip.io/)
2. Select 'Generate Error'.
    - This will send an error to the connected Sentry server.
3. Select 'View Errors' to view the errors in GitLab.

Errors may take up to a minute to appear in GitLab/Sentry.

If you need to access the Sentry project, it is located on the GitLab Sentry instance: https://sentry.gitlab.net/gitlab/tanuki-inc/.

### Generating Logs

#### Logging a combination of errors and normal logs

1. Navigate to the [demo environment](https://gitlab-examples-ops-incident-setup-everyone-tanuki-inc.34.69.64.147.nip.io/)
2. Select 'Generate Logs'.
3. This will trigger a series of logs:
    - First, it'll trigger 5 normal logs spaced 1s apart.
    - Then, 30 error logs all together.
    - Lastly, 10 more normal logs spaced 1 second apart.

#### Sending a larger number of requests (for ex. to trigger an incident)

1. Run the following command locally for 5 minutes before you'd like the incident to be generated, and leave it running until it occurs:

`ab -n 1000 -c 1 https://gitlab-examples-ops-incident-setup-everyone-tanuki-inc.34.69.64.147.nip.io/api/user/test_user`

### Generating "normal looking" logs

1.  Run the following command locally, pointed at the root URL. Running it once will generate 50 log entries:

`ab -n 50 -c 1 https://gitlab-examples-ops-incident-setup-everyone-tanuki-inc.34.69.64.147.nip.io/`

Further details and discussion about this project may be found on the original issue: https://gitlab.com/gitlab-org/gitlab/-/issues/33666.

### Continuously generate normal logs

To generate normal logs continuously, deploy https://github.com/mingrammer/flog to the kubernetes namespace of your environment (this might look like `ops-demo-app-14986497-production` for example).

The command below assumes you have connected kubectl to your cluster, see https://cloud.google.com/kubernetes-engine/docs/how-to/cluster-access-for-kubectl for help on that.

```shell
NAMESPACE=ops-demo-app-14986497-production
kubectl run flog --image=mingrammer/flog --namespace=$NAMESPACE --command -- flog -l -d1
```

## Access

Anyone may view and interact with the environment and GitLab project.

To view the error tracking page, you must have the role of Reporter or above.

## Development

Sentry integration is only switched on when the app is in production mode - a developer environment running in dev mode will not be able to send errors to sentry.

## Build Setup

```shell
# install dependencies
$ npm run install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

Made with [Nuxt.js](https://nuxtjs.org).

## Running the pipeline on GDK

When running on GDK, you must provide the build pipeline with a container registry
accessible to the GitLab runner executing the pipeline, and the Kubernetes cluster
deploying the image built by the pipeline. The steps below assume you're using the
GCP container registry (GCR), but the steps should be similar for other container
registries.

1. Clone the [project](https://gitlab.com/gitlab-examples/ops/incident-setup/everyone/tanuki-inc/) to your local GDK instance.
1. If you're using a Docker executor, you may need to
   [add the `privileged=true`](https://docs.gitlab.com/runner/executors/docker.html#the-privileged-mode)
   flag to your runner config.

1. Install Helm, Ingress and Cert-manager to your gitlab managed cluster. Note that you need
   a real email address in your cert-manager configuration, don't use the placeholder `@example.com`

1. Once Ingress is installed, set the Base Domain in the cluster administration > Details tab
   to the suggested `xxx.xxx.xxx.xxx.nip.io` value

1. Add the following variables under **Project Settings > CI / CD > Variables**:

   - `CI_REGISTRY` - If you're using the GCP container registry (GCR), use
     `https://asia.gcr.io`, `https://eu.gcr.io`, `https://us.gcr.io`, or `https://gcr.io`.

   - `CI_REGISTRY_USER` - If you're using the GCP container registry (GCR), use `_json_key`.

   - `CI_REGISTRY_PASSWORD` - If you're using GCR, the value of this variable
     should be json document generated by following the steps
     described in the [section on getting a json key for GCR](#getting-a-json-key-for-gcr).

   - `CI_APPLICATION_REPOSITORY` - If you're using GCR: `asia.gcr.io/monitoring-development-241420/rpereira2-tanuki-inc`.

     - `asia.gcr.io` is your desired GCP container registry. This variable should use the same hostname as in the
       `CI_REGISTRY` variable. If you used `https://us.gcr.io` for `CI_REGISTRY`, you should use `us.gcr.io` here.
     - `monitoring-development-241420` is the GCP project ID, available at <https://console.cloud.google.com/home/dashboard>.
     - `rpereira2-tanuki-inc` - The name of the image to be pushed to the container registry, and
       deployed to Kubernetes.

1. To reduce the number of stages in the pipeline, consider adding the following
   variables and setting them to `true`:

   - `TEST_DISABLED`
   - `CODE_QUALITY_DISABLED`
   - `LICENSE_MANAGEMENT_DISABLED`
   - `PERFORMANCE_DISABLED`
   - `SAST_DISABLED`
   - `DEPENDENCY_SCANNING_DISABLED`
   - `CONTAINER_SCANNING_DISABLED`
   - `DAST_DISABLED`
   - `REVIEW_DISABLED`

### Getting a json key for GCR

You must [create a new json key](https://cloud.google.com/container-registry/docs/advanced-authentication#json-key)
by following the steps in the link. The steps described below contain the
same information, but contain more detail based on our experience.

1. Obtain the key for the service account that will interact with Container Registry by
   opening the [service account keys page](https://console.cloud.google.com/apis/credentials/serviceaccountkey).

1. From the **Service account** list, select the service account that you want to
   use, or create a new one. Make sure the service account has the **Storage Admin** role.
   Note: Please consider giving the less privileged and more secure **Storage Object Admin**
   permission instead, scoped to only the storage bucket needed.
   See https://cloud.google.com/container-registry/docs/access-control#grant-bucket for more information.

1. Select **JSON** as the key type.

1. Click **Create**, and a JSON file containing your key downloads to your computer.

1. Run the following command to copy the contents of the JSON file as a one-liner
   (strips json pretty formatting), which you must use as the
   value of the variable `CI_REGISTRY_PASSWORD`.

   ```shell
   # note: pbcopy will only work on Mac devices
   jq -c . ~/Downloads/monitoring-development-241420-1dca162a22be.json | pbcopy
   ```

1. Don't forget to remove the JSON file from your machine, it contains sensitive passwords.

For reference, or if you want to use a different authentication method, you can
read about all the different
[authentication methods](https://cloud.google.com/container-registry/docs/advanced-authentication)
when using GCR.
