const consola = require('consola')
const chalk = require('chalk')

// Easiest way to override the throttle as the global consola instance lacks the api for it
consola._throttle = 1

class TanukiReporter extends consola.FancyReporter {
  formatLogObj(logObj) {
    // Simplified from https://github.com/nuxt/consola/blob/master/src/reporters/fancy.js
    const [message] = this.formatArgs(logObj.args).split('\n')
    const isBadge = true
    const type = this.formatType(logObj, isBadge)
    const formattedMessage = message.replace(/`([^`]+)`/g, (_, m) =>
      chalk.cyan(m)
    )
    const left = this.filterAndJoin([type, formattedMessage])
    return left
  }

  formatArgs(args) {
    const newArgs = args.map((arg) => {
      if (typeof arg === 'string') {
        return arg
      } else {
        const statusString = arg.status ? `[${arg.status}]` : ''
        const resourceString = arg.url ? `Resource '${arg.url}'` : ''
        const message = `\`${statusString}\` ${resourceString}`
        return message
      }
    })

    return super.formatArgs(newArgs)
  }
}

module.exports = TanukiReporter
