FROM node:12.13-alpine

WORKDIR /usr/src/app

ARG NODE_ENV
ENV NODE_ENV $NODE_ENV

COPY . /usr/src/app

RUN npm ci

RUN npm run build

ENV NUXT_HOST=0.0.0.0
ENV NUXT_PORT 5000
EXPOSE $NUXT_PORT

CMD [ "npm", "start" ]
